# users/urls.py
from django.urls import include, path

from rest_framework import routers
from .views import UserViewSet,activate,reset_password_view,SocialLoginView

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
        path('', include(router.urls)),
        path('auth/', include('rest_auth.urls')),
        path('activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',activate, name='activate'),
        path('password-reset/<uidb64>/<token>/', reset_password_view, name='password_reset_confirm'),
        path('social/oauth/', include('rest_framework_social_oauth2.urls')),   
        path('oauth/login/', SocialLoginView.as_view())
]
